# i3-themes

i3-default

![2018-08-03-072235_1920x1080_scrot](https://user-images.githubusercontent.com/40430516/43627980-71f6740c-96f0-11e8-88bc-0d4c41dd2b88.png)

i3-predator

![2018-07-27-130613_1920x1080_scrot](https://user-images.githubusercontent.com/40430516/43319862-30eb3c24-919e-11e8-97cc-b6468c823f52.png)

i3-material

![2018-07-24-130142_1920x1080_scrot](https://user-images.githubusercontent.com/40430516/43136854-d541daee-8f41-11e8-86eb-23b6c76a9880.png)

i3-powerline

![2018-07-23-114429_1920x1080_scrot](https://user-images.githubusercontent.com/40430516/43072451-0844465e-8e6e-11e8-829c-7758353d418b.png)

i3-gnome

![i3-gnome](https://user-images.githubusercontent.com/40430516/43042427-51296556-8d75-11e8-8829-5cdd0123635a.jpg)

i3-win

![2018-07-22-064947_1920x1080_scrot](https://user-images.githubusercontent.com/40430516/43042737-bb573240-8d7b-11e8-9c59-f9e76ccfd35d.png)
